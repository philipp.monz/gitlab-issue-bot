module.exports = {
  gitlabUrl: "https://gitlab.example.com",
  apiVersion: "4",
  privateToken: "<access_token>",
  groupId: 1,
  labelName: "inactive",
  closeAlsoIssue: false,
  daysThreshold: 700,
};
