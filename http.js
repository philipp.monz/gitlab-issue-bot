const axios = require("axios");
const config = require("./config");

class HttpSingleton {
  constructor() {
    if (!HttpSingleton.instance) {
      this._axiosInstance = axios.create({
        baseURL: config.gitlabUrl,
        headers: {
          "PRIVATE-TOKEN": config.privateToken,
        },
      });
      HttpSingleton.instance = this;
    }
    return HttpSingleton.instance;
  }

  async get(path, params = {}) {
    try {
      const response = await this._axiosInstance.get(path, { params });
      return response.data;
    } catch (error) {
      this.handleError(error);
    }
  }

  async post(path, data = {}) {
    try {
      const response = await this._axiosInstance.post(path, data);
      return response.data;
    } catch (error) {
      this.handleError(error);
    }
  }

  async put(path, data = {}) {
    try {
      const response = await this._axiosInstance.put(path, data);
      return response.data;
    } catch (error) {
      this.handleError(error);
    }
  }

  async request(config) {
    try {
      const response = await this._axiosInstance.request(config);
      return response.data;
    } catch (error) {
      this.handleError(error);
    }
  }

  handleError(error) {
    if (error.response) {
      console.error("URL or path:", error.config.url || error.config.path);
      console.error("Status code:", error.response.status);
      console.error("Error response:", error.response.data);
    } else if (error.request) {
      console.error("URL or path:", error.config.url || error.config.path);
      console.error("No response received:", error.request);
    } else {
      console.error("Error setting up request:", error.message);
    }
  }
}

module.exports = new HttpSingleton();
