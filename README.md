# :robot: GitLab Inactive Issue Labeler

This script automates the labeling of inactive issues in GitLab with the "inactive" label. It fetches issues that were last modified older than a specified threshold and adds the label to them. Optionally, it also closes issues.

Made for GitLab v16.9

For more details, refer to the interactive API documentation: GitLab OpenAPI Interactive Documentation
Interactive API documentation: https://docs.gitlab.com/ee/api/openapi/openapi_interactive.html

## Usage

1. **Clone the repository:**

   ```bash
   git clone git@git.fairkom.net:philipp.monz/gitlab-issue-bot.git
   cd gitlab-issue-bot
   ```

2. **Install dependencies:**

   ```bash
   npm install
   ```

3. **Configure the script:**

Open config.js and update the configuration variables according to your GitLab instance and preferences:

- You'll need to obtain an access token from your GitLab instance. To do this, go to your GitLab profile settings, navigate to "Access Tokens", and generate a new personal access token with the required scopes (API access). It's recommended to create a dedicated user for the bot, for example 'Gitlab Issue Bot'.

- Add user to the member with the maintainer permissions:

![add member](./howto/copygroup.png){add-member}

- Add also group Id:

![copy group-ID](./howto/addmembertogroup.png){group-ID}

4. **Run the script:**

   ```bash
   node labelingInactiveIssues.js
   ```

Note: Ensure that you have appropriate permissions and access to the GitLab instance to perform labeling operations.

5. **Automate**

Added the user: gitlabissuebot
Access Token Expires Mar 03, 2025
Credentials are in SysAdminKeypass
