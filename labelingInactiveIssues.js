const http = require("./http");
const config = require("./config");

// Function to fetch all issues in a group using pagination (limited to 100 per page)
async function getAllIssuesInGroup(groupId) {
  try {
    let allIssues = [];
    let page = 1;
    const perPage = 100;

    while (true) {
      const issues = await http.get(
        `/api/v${config.apiVersion}/groups/${groupId}/issues?per_page=${perPage}&page=${page}&state=opened`
      );

      if (issues.length === 0) {
        break;
      }

      allIssues = allIssues.concat(issues);
      page++;
    }
    return allIssues;
  } catch (error) {
    console.error("Error fetching issues in group:", error.message);
    throw error;
  }
}

// Function to set labels for an issue in a project
async function setLabelForIssue(projectId, issueId, labels) {
  try {
    const label = await http.put(
      `/api/v${config.apiVersion}/projects/${projectId}/issues/${issueId}`,
      { labels: labels }
    );

    return label;
  } catch (error) {
    console.error("Error:", error.message);
    throw error;
  }
}

// Function retrieves inactive issues in a group that haven't been updated for a specified number of days.
async function getInactiveIssuesInGroupOlderThanDays(groupId, daysThreshold) {
  try {
    console.log("loading issues of group", groupId);
    const groupIssues = await getAllIssuesInGroup(groupId);
    console.log(groupIssues.length, "Group issues loaded");
    const inactiveIssues = groupIssues.filter((issue) => {
      const issueDate = new Date(issue.updated_at);
      const thresholdDate = new Date();
      thresholdDate.setDate(thresholdDate.getDate() - daysThreshold);
      return issueDate < thresholdDate;
    });

    console.log(
      inactiveIssues.length,
      "issues were last active more than",
      daysThreshold,
      "days ago"
    );

    return inactiveIssues;
  } catch (error) {
    console.error("Error:", error.message);
    throw error;
  }
}

// Function sets a specific label
async function setSpecificLabel(issues) {
  try {
    for (const issue of issues) {
      const issueLabels = issue.labels;
      if (!issueLabels.includes(config.labelName)) {
        issueLabels.push(config.labelName);
        var stringLabels = issueLabels.join(",");
        await setLabelForIssue(issue.project_id, issue.iid, stringLabels);
        console.log("Issue", issue.iid, "labels updated successfully.");
      }

      if (config.closeIssues) {
        await http.put(
          `/api/v${config.apiVersion}/projects/${issue.project_id}/issues/${issue.iid}`,
          { state_event: "close" }
        );
        console.log("Issue", issue.iid, "closed successfully.");
      }
    }

    console.log("Issues updated successfully.");
  } catch (error) {
    console.error("Error:", error.message);
    throw error;
  }
}

(async () => {
  try {
    const inactiveIssues = await getInactiveIssuesInGroupOlderThanDays(
      config.groupId,
      config.daysThreshold
    );
    if (inactiveIssues.length > 0) {
      console.log(inactiveIssues.length);
      await setSpecificLabel(inactiveIssues);
    }
  } catch (error) {
    console.error("Error:", error.message);
  }
})();
